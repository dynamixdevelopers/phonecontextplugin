/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.phonecontext;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Looper;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import org.ambientdynamix.api.application.ErrorCodes;
import org.ambientdynamix.api.contextplugin.*;
import org.ambientdynamix.contextplugins.ambientcontrol.*;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * Example auto-reactive plug-in that detects the device's battery level.
 *
 * @author Darren Carlson
 */
public class PhoneContextPluginRuntime extends ContextPluginRuntime {
    private static final int VALID_CONTEXT_DURATION = 60000;
    // Static logging TAG
    private final String TAG = this.getClass().getSimpleName();
    // Our secure context
    private Context context;
    private TelephonyManager telephonyManager;
    ControlConnectionManager controlMgr;
    private ContextPluginRuntime runtime;

    private PhoneStateListener phoneStateListener = new PhoneStateListener() {

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                sendBroadcastContextEvent(new PhoneContextInfo(telephonyManager, incomingNumber));
                if(sendUpdates)
                    controlMgr.sendControllCommand(new ToggleCommand(Commands.SWITCH,true,"Call Activity"),
                        runtime.getPluginFacade().getPluginInfo(getSessionId()).getPluginId());
            } else if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                if(sendUpdates)
                controlMgr.sendControllCommand(new ToggleCommand(Commands.SWITCH,true,"Call Activity"),
                        runtime.getPluginFacade().getPluginInfo(getSessionId()).getPluginId());
            } else if (TelephonyManager.CALL_STATE_IDLE == state) {
                if(sendUpdates)
                controlMgr.sendControllCommand(new ToggleCommand(Commands.SWITCH, false, "Call Activity"),
                        runtime.getPluginFacade().getPluginInfo(getSessionId()).getPluginId());
            }
        }


    };

    private boolean sendUpdates = false;
    ControlConnectionManager.IControllConectionManagerListener controlListener = new ControlConnectionManager.IControllConectionManagerListener() {


        @Override
        public void controllRequest(String s, String s1) {
            sendUpdates = true;
        }

        @Override
        public void stopControlling(String s, String s1) {
            sendUpdates = false;
        }

        @Override
        public boolean consumeCommand(IControlMessage command, String sourcePluginId, String requestId) {
            return false;
        }

        @Override
        public Set<String> getDeviceAdresses() {
            return new HashSet<String>();
        }
    };

    /**
     * Called once when the ContextPluginRuntime is first initialized. The implementing subclass should acquire the
     * resources necessary to run. If initialization is unsuccessful, the plug-ins should throw an exception and release
     * any acquired resources.
     */
    @Override
    public void init(PowerScheme powerScheme, ContextPluginSettings settings) throws Exception {
        // Set the power scheme
        this.setPowerScheme(powerScheme);
        // Store our secure context
        this.context = this.getSecuredContext();
        telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        controlMgr = new ControlConnectionManager(this, controlListener, TAG);
        runtime = this;
    }


    @Override
    public boolean addContextlistener(ContextListenerInformation listenerInfo) {
        return controlMgr.addContextListener(listenerInfo);
    }

    @Override
    public void onMessageReceived(Message message) {
        controlMgr.handleConfigCommand(message);
    }

    /**
     * Called by the Dynamix Context Manager to start (or prepare to start) context sensing or acting operations.
     */
    @Override
    public void start() {

        int interestedEvents = PhoneStateListener.LISTEN_CALL_STATE | PhoneStateListener.LISTEN_CELL_LOCATION | PhoneStateListener.LISTEN_DATA_ACTIVITY | PhoneStateListener.LISTEN_DATA_CONNECTION_STATE | PhoneStateListener.LISTEN_SERVICE_STATE;
        telephonyManager.listen(phoneStateListener, interestedEvents);
        Log.d(TAG, "Started!");
    }

    /**
     * Called by the Dynamix Context Manager to stop context sensing or acting operations; however, any acquired
     * resources should be maintained, since start may be called again.
     */
    @Override
    public void stop() {
        // Unregister battery level changed notifications
        telephonyManager.listen(phoneStateListener, PhoneStateListener.LISTEN_NONE);
        Log.d(TAG, "Stopped!");
    }

    /**
     * Stops the runtime (if ) and then releases all acquired resources in preparation for garbage collection.
     * Once this method has been called, it may not be re-started and will be reclaimed by garbage collection sometime
     * in the indefinite future.
     */
    @Override
    public void destroy() {
        this.stop();
        context = null;
        Log.d(TAG, "Destroyed!");
    }

    @Override
    public void handleContextRequest(UUID requestId, String contextType) {

        // Check for proper context type
        if (contextType.equalsIgnoreCase(PhoneContextInfo.CONTEXT_TYPE)) {


            // Send the context event
            sendContextEvent(requestId, new PhoneContextInfo(telephonyManager, ""), VALID_CONTEXT_DURATION);
        } else {
            sendContextRequestError(requestId, "CONTEXT_SUPPORT_NOT_FOUND for " + contextType, ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
        }
    }

    @Override
    public void handleConfiguredContextRequest(final UUID requestId, String contextType, Bundle config) {
        if (!controlMgr.handleConfiguredContextRequest(requestId, contextType, config)) {
            if (contextType.equals("org.ambientdynamix.contextplugins.phonecontext.phonecall")) {
                //Initiate phone call
                Log.w(TAG, "Initiating Phone call");
                try {
                    if (Looper.myLooper() == null)
                        Looper.prepare();
                    PhoneStateListener callStateListener = new PhoneStateListener() {

                        boolean callOngoing;

                        @Override
                        public void onCallStateChanged(int state, String incomingNumber) {

                            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                                // active
                                Log.i(TAG, "OFFHOOK");
                                callOngoing = true;
                                // send context event that phone call is over
                                Bundle calling = new Bundle();
                                calling.putBoolean("CALLING", true);

                                runtime.sendContextEvent(requestId, calling, "org.ambientdynamix.contextplugins.phonecontext.callstate");

                            } else if (TelephonyManager.CALL_STATE_RINGING == state) {
                                // send context event that phone call is over
                                Bundle callDone = new Bundle();
                                callDone.putBoolean("CALLING", false);

                                runtime.sendContextEvent(requestId, callDone, "org.ambientdynamix.contextplugins.phonecontext.callstate");
                            } else if (TelephonyManager.CALL_STATE_IDLE == state) {
                                // run when class initial and phone call ended,
                                // need detect flag from CALL_STATE_OFFHOOK
                                Log.i(TAG, "IDLE");

                                if (callOngoing) {

                                    Log.i(TAG, "restart app");

                                    // send context event that phone call is over
                                    Bundle callDone = new Bundle();
                                    callDone.putBoolean("CALLING", false);

                                    runtime.sendContextEvent(requestId, callDone, "org.ambientdynamix.contextplugins.phonecontext.callstate");

                                    callOngoing = false;
                                    telephonyManager.listen(this, PhoneStateListener.LISTEN_NONE);
                                }
                            }
                        }
                    };
                    telephonyManager.listen(callStateListener, PhoneStateListener.LISTEN_CALL_STATE);

                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse(config.getString("PHONE_NUMBER")));
                    callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(callIntent);

                } catch (ActivityNotFoundException activityException) {
                    Log.e(TAG, "Phone Call failed", activityException);
                }
            } else if (contextType.equals(PhoneContextInfo.CONTEXT_TYPE)) {
                sendContextEvent(requestId, new PhoneContextInfo(telephonyManager, ""), VALID_CONTEXT_DURATION);
                Log.e(TAG, "delete me on sight");

            } else if (contextType.equals("org.ambientdynamix.contextplugins.phonecontext.smsmessage")) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("sms:" + config.get("PHONE_NUMBER")));
                intent.putExtra("sms_body", (String) config.get("SMS_MESSAGE"));
                context.startActivity(intent);

            } else {
                sendContextRequestError(requestId, "CONTEXT_SUPPORT_NOT_FOUND for " + contextType, ErrorCodes.CONTEXT_SUPPORT_NOT_FOUND);
            }
        }


    }


    @Override
    public void setPowerScheme(PowerScheme scheme) {
        // Not supported
    }

}