# The Phone Context Plugin
This plugin enables the user to make calls, send sms and/or get notified of incoming calls.

Interaction can be performed through the Ambient Control mechanisms, See [Ambient Control documentation](https://bitbucket.org/dynamixdevelopers/ambientcontrolplugin) or through regular context request, to make calls or send SMS

The plugins control description is as follows:
```JSON
{
    "name":"Phone Context Plugin",
    "artifact_id":"org.ambientdynamix.contextplugins.phonecontext",
    "description":"",
    "pluginVersion":"1.0.0",
    "owner_id":"",
    "platform":"",
    "minPlatformVersion":"",
    "minFrameworkVersion":"",
    "inputList":[],
    "outputList":{
        "Call Activity":"SWITCH"
    },
    "optionalInputList":[]
}
```

##Supported Context Types
```
org.ambientdynamix.contextplugins.phonecontext
org.ambientdynamix.contextplugins.phonecontext.phonecall("PHONE_NUMBER")
org.ambientdynamix.contextplugins.phonecontext.smsmessage("PHONE_NUMBER","SMS_MESSAGE")
```