/*
 * Copyright (C) The Ambient Dynamix Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.ambientdynamix.contextplugins.phonecontext;

import java.util.HashSet;
import java.util.Set;

import android.content.Intent;
import android.os.BatteryManager;
import android.os.Parcel;
import android.os.Parcelable;
import android.telephony.TelephonyManager;

class PhoneContextInfo implements IPhoneContextInfo{
	/**
	 * Required CREATOR field that generates instances of this Parcelable class from a Parcel.
	 *
	 * @see http://developer.android.com/reference/android/os/Parcelable.Creator.html
	 */
	public static Parcelable.Creator<PhoneContextInfo> CREATOR = new Parcelable.Creator<PhoneContextInfo>() {
		/**
		 * Create a new instance of the Parcelable class, instantiating it from the given Parcel whose data had
		 * previously been written by Parcelable.writeToParcel().
		 */
		public PhoneContextInfo createFromParcel(Parcel in) {
			return new PhoneContextInfo(in);
		}

		/**
		 * Create a new array of the Parcelable class.
		 */
		public PhoneContextInfo[] newArray(int size) {
			return new PhoneContextInfo[size];
		}
	};
	// Public static variable for our supported context type
	public static String CONTEXT_TYPE = "org.ambientdynamix.contextplugins.phonecontext";

    // Private data
    private int callState;
    private int dataActivityState;
    private int dataState;
    private int networkType;
    private int phoneType;
    private int simState;
    private String incomingNumber;

    public PhoneContextInfo(TelephonyManager telephonyManager,String incomingNumber) {
        callState = telephonyManager.getCallState();
        dataActivityState = telephonyManager.getDataActivity();
        dataState = telephonyManager.getDataState();
        networkType = telephonyManager.getNetworkType();
        phoneType = telephonyManager.getPhoneType();
        simState = telephonyManager.getSimState();
        this.incomingNumber = incomingNumber;
    }

	/**
	 * Returns the type of the context information represented by the IContextInfo. This string must match one of the
	 * supported context information type strings described by the source ContextPlugin.
	 */
	@Override
	public String getContextType() {
		return CONTEXT_TYPE;
	}

	/**
	 * Returns the fully qualified class-name of the class implementing the IContextInfo interface. This allows Dynamix
	 * applications to dynamically cast IContextInfo objects to their original type using reflection. A Java
	 * "instanceof" compare can also be used for this purpose.
	 */
	@Override
	public String getImplementingClassname() {
        return this.getClass().getName();
	}

	/**
	 * Returns a Set of supported string-based context representation format types or null if no representation formats
	 * are supported. Examples formats could include MIME, Dublin Core, RDF, etc. See the plug-in documentation for
	 * supported representation types.
	 */
	@Override
	public Set<String> getStringRepresentationFormats() {
		Set<String> formats = new HashSet<String>();
		formats.add("text/plain");
		return formats;
	}

	/**
	 * Returns a string-based representation of the IContextInfo for the specified format string (e.g.
	 * "application/json") or null if the requested format is not supported.
	 */
	@Override
	public String getStringRepresentation(String format) {
		if (format.equalsIgnoreCase("text/plain"))
			return "Phone State: " + getCallStateString(callState) + " " + getDataStateString(dataState) + " "
                    + getDataActivityStateString(dataActivityState) + " " + getSimStateString(simState);
		else
			// Format not supported, so return an empty string
			return "";
	}



	/**
	 * Used by Parcelable when sending (serializing) data over IPC.
	 */
	public void writeToParcel(Parcel out, int flags) {
        out.writeInt(callState);
        out.writeInt(dataActivityState);
        out.writeInt(dataState);
        out.writeInt(networkType);
        out.writeInt(phoneType);
        out.writeInt(simState);
        out.writeString(incomingNumber);

	}

	/**
	 * Used by the Parcelable.Creator when reconstructing (deserializing) data sent over IPC.
	 */
	private PhoneContextInfo(final Parcel in) {
        callState = in.readInt();
        dataActivityState = in.readInt();
        dataState = in.readInt();
        networkType = in.readInt();
        phoneType = in.readInt();
        simState = in.readInt();
        incomingNumber = in.readString();
	}

	/**
	 * Default implementation that returns 0.
	 *
	 * @return 0
	 */
	@Override
	public int describeContents() {
		return 0;
	}

    @Override
    public int getCallState() {
        return callState;
    }

    @Override
    public int getDataActivityState() {
        return dataActivityState;
    }

    @Override
    public int getDataState() {
        return dataState;
    }

    @Override
    public int getNetworkType() {
        return networkType;
    }

    @Override
    public int getPhoneType() {
        return phoneType;
    }

    @Override
    public int getSimState() {
        return simState;
    }

    public static String getCallStateString(int callState){
        switch(callState)
        {
            case TelephonyManager.CALL_STATE_IDLE:
                return "CALL_STATE_IDLE";
           case TelephonyManager.CALL_STATE_OFFHOOK:
                    return "CALL_STATE_OFFHOOK";
           case TelephonyManager.CALL_STATE_RINGING:
                    return "CALL_STATE_RINGING";
           default:
               return "";
        }
    }

    public static String getDataActivityStateString(int dataActivityState)
    {
        switch (dataActivityState)
        {
            case TelephonyManager.DATA_ACTIVITY_NONE:
                return "DATA_ACTIVITY_NONE";
            case TelephonyManager.DATA_ACTIVITY_IN:
                return "DATA_ACTIVITY_IN";
            case TelephonyManager.DATA_ACTIVITY_OUT:
                return "DATA_ACTIVITY_OUT";
            case TelephonyManager.DATA_ACTIVITY_INOUT:
                return "DATA_ACTIVITY_INOUT";
            case TelephonyManager.DATA_ACTIVITY_DORMANT:
                return "DATA_ACTIVITY_DORMANT";
            default:
                return "";

        }
    }

    public static String getDataStateString(int dataState)
    {
        switch (dataState)
        {
            case TelephonyManager.DATA_DISCONNECTED:
                return "DATA_DISCONNECTED";
            case TelephonyManager.DATA_CONNECTING:
                return "DATA_CONNECTING";
            case TelephonyManager.DATA_CONNECTED:
                return "DATA_CONNECTED";
            case TelephonyManager.DATA_SUSPENDED:
                return "DATA_SUSPENDED";
            default:
                return "";
        }
    }

    public static String getSimStateString(int simState){
        switch (simState){
            case TelephonyManager.SIM_STATE_ABSENT:
                return "SIM_STATE_ABSENT";
        case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
                return "SIM_STATE_NETWORK_LOCKED";
        case TelephonyManager.SIM_STATE_PIN_REQUIRED:
                return "SIM_STATE_PIN_REQUIRED";
        case TelephonyManager.SIM_STATE_READY:
                return "SIM_STATE_READY";
        case TelephonyManager.SIM_STATE_PUK_REQUIRED:
                return "SIM_STATE_PUK_REQUIRED";
        case TelephonyManager.SIM_STATE_UNKNOWN:
                return "SIM_STATE_UNKNOWN";
            default:
                return "";
        }
    }
}