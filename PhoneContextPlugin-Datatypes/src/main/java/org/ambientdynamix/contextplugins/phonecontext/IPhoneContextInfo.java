package org.ambientdynamix.contextplugins.phonecontext;

import android.telephony.TelephonyManager;
import org.ambientdynamix.api.application.IContextInfo;

/**
 * Created by workshop on 11/5/13.
 */
public interface IPhoneContextInfo extends IContextInfo {

    public int getCallState();
    public int getDataActivityState();
    public int getDataState();
    public int getNetworkType();
    public int getPhoneType();
    public int getSimState();

}
